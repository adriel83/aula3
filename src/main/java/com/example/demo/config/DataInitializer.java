package com.example.demo.config;

import com.example.demo.entity.*;
import com.example.demo.repository.*;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class DataInitializer implements ApplicationListener<ContextRefreshedEvent>{

    @Autowired
    private UserRepository userRepository;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent e) {
        List<User> users = userRepository.findAll();
        if (users.isEmpty()) {
            users.add(new User("usuario", "123"));
            users.add(new User("シャナ", "132"));
            userRepository.saveAll(users);
        }
    }
}
